## Build the image
docker build -t vasin/rails .
## Run the image locally with bash to make some tests
docker run -i -t vasin/rails /bin/bash
## Push the image back to the Docker Hub
docker push vasin/rails

Original source: https://hub.docker.com/r/spittet/ruby-postgresql/
